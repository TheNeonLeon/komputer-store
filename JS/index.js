//Global variables
let payBalance = 0;
let bankBalance = 0;
let outstandingLoanAmount = 0;
let loanAmount = 0;
let computers = [];
let cart = [];
let baseImageUrl = "https://noroff-komputer-store-api.herokuapp.com/";
let isLoanActive = false;

//Global variables
const computerElements = document.getElementById("select-laptop");
const computerPrice = document.getElementById("computerPrice");
const computerSpecs = document.getElementById("computerSpecs");
const computerImage = document.getElementById("computerImage");
const computerTitle = document.getElementById("computerTitle");
const computerDescription = document.getElementById("computerDescription");
const buyButton = document.getElementById("buy-button");
const loanButton = document.getElementById("loanButton");
const outstandingBalance = document.getElementById("outstandingLoanAmount");
const repayLoanButton = document.getElementById("repayLoan");

//Get money for working function
function work() {
    console.log("hello");
     payBalance += 100;

     document.getElementById("payBalance").innerText = new Intl.NumberFormat('sv-SE', { style: 'currency', currency: 'SEK' }).format(payBalance);
    
}

//send the current pay balance to the bank
function sendBalance(){

    if(outstandingLoanAmount){
        let sendToPayCalculation = (payBalance * 0.9);
        let sendToOutstandingLoanCalculation = (payBalance * 0.1);
        
        bankBalance += sendToPayCalculation;
        outstandingLoanAmount -= sendToOutstandingLoanCalculation; 
        
        document.getElementById("outstandingLoanAmount").innerText = new Intl.NumberFormat('sv-SE', { style: 'currency', currency: 'SEK' }).format(outstandingLoanAmount);
        document.getElementById("bankBalance").innerText = new Intl.NumberFormat('sv-SE', { style: 'currency', currency: 'SEK' }).format(bankBalance);
        document.getElementById("payBalance").innerText = 0;
     }
     else if(payBalance){
        bankBalance += payBalance;
        document.getElementById("bankBalance").innerText = new Intl.NumberFormat('sv-SE', { style: 'currency', currency: 'SEK' }).format(bankBalance);
        document.getElementById("payBalance").innerText = 0;
    }
    else{
        alert("insufficient funds");
    }
    payBalance *= 0;

}

//Get a loan and the send the amount to the bank
const getLoan = function(){
   let promptAmount = Number(window.prompt("Enter the amount you would like to loan out"));

    
   if(!bankBalance){
        alert("You cannot get a loan");
    }else if(outstandingLoanAmount > 0){
        alert("you must repay loan");
    }

   else if(parseInt(promptAmount) <= bankBalance * 2){
    bankBalance += parseInt(promptAmount);
    document.getElementById("bankBalance").innerHTML = new Intl.NumberFormat('sv-SE', { style: 'currency', currency: 'SEK' }).format(bankBalance);

    document.getElementById("outstandingLoanAmount").style.display = "flex";
    document.getElementById("repayLoan").style.display = "flex";
    document.getElementById("outstandingLoanAmount").innerText = outstandingLoanAmount += promptAmount;

   }
   else if(parseInt(promptAmount) > bankBalance * 2){
        alert("You cannot get a loan");
    }
    else{
        alert("something went wrong");
    }

    isLoanActive = true;
}

//send whole pay amount towards outstanding loan amount, using repay loan button
let remainingBalance = outstandingLoanAmount - payBalance;
const repayLoan = function() {
    // if(outstandingLoanAmount <= 0){
    // }
    if(isLoanActive){
        outstandingLoanAmount -= payBalance
        document.getElementById("outstandingLoanAmount").innerText = new Intl.NumberFormat('sv-SE', { style: 'currency', currency: 'SEK' }).format(outstandingLoanAmount);
        payBalance *= 0;
        document.getElementById("payBalance").innerText = new Intl.NumberFormat('sv-SE', { style: 'currency', currency: 'SEK' }).format(payBalance);
    }if(outstandingLoanAmount == 0){
        isLoanActive = false;
        document.getElementById("repayLoan").style.display = "none";
        
    }

   if(outstandingLoanAmount < 0){
         
         bankBalance += outstandingLoanAmount * (-1)
         outstandingLoanAmount = 0;
         document.getElementById("outstandingLoanAmount").innerText = new Intl.NumberFormat('sv-SE', { style: 'currency', currency: 'SEK' }).format(remainingBalance);
         document.getElementById("bankBalance").innerText = bankBalance;
         isLoanActive = false;
         document.getElementById("repayLoan").style.display = "none";

        
     }

}

repayLoanButton.addEventListener("click", repayLoan);
loanButton.addEventListener("click", getLoan);

//Get data from api
function getData(){
    fetch('https://noroff-komputer-store-api.herokuapp.com/computers')
    .then(response => response.json())
    .then(data => {
        computers = data;
        console.log(data);
        console.log(cart);
    })
    .then(computers => addComputersToMenu(computers))
    .catch((error) => {
        console.log("Something went wrong" + error);
    })
    
}

getData();

function addComputersToMenu() {
    computers.forEach(element => addComputerToMenu(element));
}

//add computers to option menu
function addComputerToMenu(computer){
    const computerElement = document.createElement("option");
    computerElement.value = computer.id;
    computerElement.appendChild(document.createTextNode(computer.title));
    computerElements.appendChild(computerElement);

}

let checkComputerPrice;

//add selected computer from option menu to laptop section
function addComputerToCart(element){
    const selectedComputer = computers[element.target.selectedIndex];
    computerPrice.innerText =  new Intl.NumberFormat('sv-SE', { style: 'currency', currency: 'SEK' }).format(selectedComputer.price);
    
    computerSpecs.innerText = selectedComputer.specs;
    computerTitle.innerText = selectedComputer.title;
    computerDescription.innerText = selectedComputer.description;
    computerImage.src = baseImageUrl + selectedComputer.image;

    checkComputerPrice = selectedComputer.price;
    console.log(computerPrice);
    console.log(selectedComputer);
    console.log(computerSpecs);
}
//buy computer functionality
buyButton.addEventListener('click', () => {
    
    if(bankBalance >= checkComputerPrice){
        bankBalance -= checkComputerPrice;
        document.getElementById("bankBalance").innerText = `${bankBalance} kr`;
        alert('You bought a computer!');
    }else{
        alert('You cannot afford that');
    }
})

computerElements.addEventListener("change", addComputerToCart);