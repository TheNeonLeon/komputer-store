# Komputer store

## How to start project
* Navigate to index.html
* Open index.html with live-server

## Description
The web app has 4 differen components where you can earn money and put the money earned in the bank. You can browse through different computers which you can then use to buy a computer with.
The web app has features such as:
- Keeping track of money earned through working
- Keeping track of money sent to the bank
- Loan money from the bank
- Keeping track of outstanding loan amount
- Paying of the outstanding loan amount by working
- Browsing different types of computers, which you can then buy with the amount you have in bank balance
## License
For open source projects, say how it is licensed.